'use strict'

function toggle_extra_options() {
    let memory = $('#batch_connect_session_context_memory');
    let cores = $('#batch_connect_session_context_cores');
    let gpus = $('#batch_connect_session_context_gpus');
    let comment = $('#batch_connect_session_context_comment');
    let slurm_options = $('#batch_connect_session_context_slurm_options');
    let extra_options_checkbox = document.getElementById("batch_connect_session_context_extra_options"); 
if(extra_options_checkbox.checked == true)  {
    memory.parent().show();
    cores.parent().show();
    gpus.parent().show();
    comment.parent().show();
    slurm_options.parent().show();
  } else {
    memory.parent().hide();
    cores.parent().hide();
    gpus.parent().hide();
    comment.parent().hide();
    slurm_options.parent().hide();
  }
}

toggle_extra_options()

const checkbox = document.getElementById("batch_connect_session_context_extra_options"); 

checkbox.addEventListener('change', (event) => {
  if (event.currentTarget.checked) {
    toggle_extra_options();
  } else {
    toggle_extra_options();
  }
})